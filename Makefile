cpp_src = $(wildcard *.cpp)
cpp_src += $(wildcard ./X11/*.cpp)
cc_src =  $(wildcard *.cc)
obj = $(cpp_src:.cpp=.o) $(cc_src:.cc=.o)
dep = $(obj:.o=.d)

EXECUTABLE = tracktor_imit
PACKAGES = 
LIBS= -lX11 -lpthread
CXXFLAGS += -g3 -O0 -Wall -std=c++11 -pedantic -I./
ifneq ($(PACKAGES),)
CXXFLAGS += $(shell pkg-config $(PACKAGES) --cflags)
LDFLAGS += $(shell pkg-config $(PACKAGES) --libs) 
endif

LDFLAGS += $(LIBS)
PREFIX?=/usr/local

# view:
# 	@echo src $(src)
# 	@echo obj $(obj)
all:$(EXECUTABLE)

$(EXECUTABLE): $(obj)
	$(CXX) -o $@ $^ $(LDFLAGS)

-include $(dep)

%.d: %.cpp
	@$(CXX) $(CFLAGS) $< -MM -MT $(@:.d=.o) >$@
.PHONY: clean
clean: cleandep
	rm -f $(obj) $(EXECUTABLE)

.PHONY: cleandep
cleandep:
	rm -f $(dep)
install:all
	install ./$(EXECUTABLE) $(PREFIX)/bin
remove:
	rm -f $(PREFIX)/bin/$(EXECUTABLE)

test: test.c
	g++ -o $@ -lX11 -lm $^
