#include <stdlib.h>
#include <string>
#include <stdio.h>
#include <string.h>
#include <strings.h>

#include "txlib_ui.h"
#include <defines.h>
#include <tkernel.h>
#include <tinfo.h>
txlib_ui::txlib_ui(class tkernel *_k,bool _draw_info)
:tui(_k,_draw_info),win_id(0),mw_gc(0)
{
	DBG(__PRETTY_FUNCTION__);
	dpy = XOpenDisplay(NULL);
	if (dpy == NULL) 
	{
		throw std::string("Could not open display");
	}
	XSetErrorHandler(&error_handler);
	screen = DefaultScreen(dpy);
}

txlib_ui::~txlib_ui()
{
	DBG(__PRETTY_FUNCTION__);
// 	stop();
}

void txlib_ui::close()
{
	DBG(__PRETTY_FUNCTION__);
	if (dpy != NULL) 
	{
		if(win_id)
		{
			DBG("Destroying window");
			XDestroyWindow(dpy,win_id);
			win_id = 0;
		}
		
		XCloseDisplay(dpy);
		dpy = NULL;
	}
	gui_thread.join();
}
void txlib_ui::update()
{
	if(win_id)
	{
		XEvent ev;
		bzero(&ev,sizeof(ev));
		ev.type = Expose;
		ev.xexpose.window = win_id;
		XLockDisplay(dpy);
        XSendEvent(dpy,win_id,False,ExposureMask,&ev);
        XFlush(dpy);
		XUnlockDisplay(dpy);
        

	}
}

int txlib_ui::show(int w,int h)
{
	width = w;
	height = h;
	DBG(__PRETTY_FUNCTION__);
	XInitThreads();
	start_gui_thread();
	
	return 0;
}

int txlib_ui::error_handler(Display *d, XErrorEvent* err)
{
	ERR("Handled error:");
	ERR("Error code "<<err->error_code<<", major req "<<err->request_code<<", minor req "<<err->minor_code);
	char error[1024];
	bzero(error,1024);
	XGetErrorText(d,err->error_code,error,1024);
	ERR("Error message "<<error);
	return 0;
}

void txlib_ui::gui_handler()
{
	XEvent e;
	DBG("Start gui thread");
	Display *thread_dpy = XOpenDisplay(NULL);
	if (thread_dpy == NULL) 
	{
		//TODO корректно обрабатывать ошибку!
		throw std::string("Could not open display");
	}
	
	
	
	win_id = XCreateSimpleWindow(thread_dpy,RootWindow(thread_dpy,screen),0,0,width,height,2,BlackPixel(thread_dpy, screen),WhitePixel(thread_dpy, screen));
	mw_gc = DefaultGC(thread_dpy, screen);
	
	
	
	Atom delWindow = XInternAtom( thread_dpy, "WM_DELETE_WINDOW", 0 );
	XSetWMProtocols(thread_dpy , win_id, &delWindow, 1);
	
	XSelectInput(thread_dpy , win_id, ExposureMask | KeyPressMask | StructureNotifyMask);
	
	XMapWindow(thread_dpy, win_id);
	
	DBG("gui main loop");
	bool running = 1;
	while(running) 
	{
		XNextEvent(thread_dpy, &e);
		switch(e.type)
		{
			case DestroyNotify:
			{
				running = 0;
				break;
			}
			case ClientMessage:
			{
				if (Atom(e.xclient.data.l[0]) != delWindow)
					break;
			}
			case KeyPress:
			{
				ui_keys k;
				unsigned int code = XLookupKeysym(&e.xkey, 0);
				switch(code)
				{
					case 0x0020:k = key_space;break;
					case 0xff52:k = key_up;break;
					case 0xff0d:k = key_enter;break;
					case 0xff54:k = key_down;break;
					case 0xff51:k = key_left;break;
					case 0xff53:k = key_right;break;
					case 0xff1b:k = key_esc;break;
					default:k = key_any;
					
				}
				kernel->key_pressed(k);
				break;
			}
			case ConfigureNotify:
			{
				XConfigureEvent *ce =  (XConfigureEvent *)&e;
				if (ce->width != width || ce->height != height) 
				{
					width = ce->width;
					height = ce->height;
					update();
				}
				
			}
			case Expose: 
			{
				XExposeEvent *xe = (XExposeEvent *)&e;
				XClearArea (thread_dpy,win_id, 0, 0, 0, 0, False);
				if(tracktor)
				{
					tracktor_mutex.lock();
					XDrawLines(thread_dpy,xe->window,mw_gc,tracktor,BODY_LENGTH + 1,CoordModeOrigin);
					tracktor_mutex.unlock();
				}
				if(draw_info)
				{
					info = kernel->info->get_info();
					int h = 10;
					for(auto it:info)
					{
						XDrawString(thread_dpy,xe->window,mw_gc,10,h,it.c_str(),it.length());
						h+=10;
					}
				}
				help_mutex.lock();
				if(help.size())
				{
					int h = 10;
					for(auto it:help)
					{
						XDrawString(thread_dpy,xe->window,mw_gc,10,h,it.c_str(),it.length());
						h+=10;
					}
				}
				help_mutex.unlock();

				break;
			}
			
		};
	}
	DBG("Stop gui thread");
}



