#ifndef TXLIB_UI_H
#define TXLIB_UI_H
#include <X11/Xlib.h>
#include <tui.h>
class txlib_ui: public tui
{
public:
    txlib_ui(class tkernel *_k,bool _draw_info);
    virtual ~txlib_ui();
	int show(int w,int h) override;
	void close() override;
	void update() override;
private:
	Window win_id;
	GC mw_gc;
	GC info_gc;
	Display *dpy;
	int screen;
	static int error_handler(Display *d,XErrorEvent *err);
	void gui_handler() override;
	
};

#endif // TXLIB_UI_H
