#ifndef __DEFINES__
#define __DEFINES__
#include <iostream>
#define ERR(x) std::cerr<<x<<std::endl
#define DBG(x) std::cout<<x<<std::endl
#define DBG_NO_ENDL(x) std::cout<<x
#define DBG_ENDL std::cout<<std::endl

#if defined __unix__ || defined __linux__
#include <X11/Xlib.h>
typedef XPoint tpoint;
#else
#error "Здесь сделать реализацию под Windows"
#endif

std::ostream& operator << (std::ostream &stream,const tpoint &obj);
tpoint operator+(const tpoint &p1,const tpoint &p2);
tpoint operator-(const tpoint &p1,const tpoint &p2);
tpoint &operator+=(tpoint &left,const tpoint &val);
bool operator==(const tpoint &left,const tpoint &val);
#define TRACKTOR_H 4.
#define BODY_LENGTH 3
#define _A 6378137.0
#define _A_2 _A*_A

#define _B 6356752.3142
#define _B_2 (_B*_B)
#define _F ((_A - _B)/_A)
#define _E_R2 0.00673949674228
//((_A_2 - _B_2)/(_B_2))

// #define _E 0.0818
#define _E_2 0.00669437999014
//0.00669437999014
#define _E_4 (_E_2*_E_2)

#define LOCAL_LAT 0.974754422565989
#define LOCAL_LON 0.974754422565989


enum ui_keys
{
	key_enter,
	key_space,
	key_left,
	key_right,
	key_up,
	key_down,
	key_esc,
	key_any

};

#endif
