#include <iostream>
#include <unistd.h>
#include <math.h>
#include "defines.h"
#include "tkernel.h"
int main(int argc, char **argv)
{
	DBG("Enter programm");
	if(argc != 2)
	{
		ERR("Usage: tracktor_imit <path to data file>");
		return -1;
	}
	tkernel kernel(argv[1]);
	kernel.start();
	
	
	return 0;
}
