#include <math.h>
#include "tinfo.h"
#include "tkernel.h"
tinfo::tinfo(class tkernel *_k)
:kernel(_k)
{

}

tinfo::~tinfo()
{

}

std::vector<std::string> tinfo::get_info()
{
	std::unique_lock<decltype(info_mutex)> lock(info_mutex);

	std::vector<std::string> res;
	double grad;
	double min;
	double sec;
	
	std::string cv = "Source:    " + sender;
	res.push_back(cv);
	
	
	cv = "Time:      " + std::to_string(time);
	res.push_back(cv);
	cv = "North:     " + std::to_string(ned_pos.x1);
	res.push_back(cv);
	
	cv = "Speed:     " + std::to_string(speed);
	res.push_back(cv);
	
	cv = "East:      " + std::to_string(ned_pos.x0);
	res.push_back(cv);
	
	
	grad = 180*asin(sin_azimuth)/M_PI;
	min = (grad - (int)grad)*60.;
	sec = (min - (int)min)*60.;
	cv = "Azimuth:   " + std::to_string(int(grad)) + "* " + std::to_string(int(min)) + "' " +  std::to_string(int(sec)) + "\"";
	res.push_back(cv);
	
	grad = 180*steering_angle/M_PI;
	min = (grad - (int)grad)*60.;
	sec = (min - (int)min)*60.;
	cv = "Steering:  " + std::to_string(int(grad)) + "* " + std::to_string(int(min)) + "' " +  std::to_string(int(sec)) + "\"";
	res.push_back(cv);
	
	grad = 180*angular_speed/M_PI;
	min = (grad - (int)grad)*60.;
	sec = (min - (int)min)*60.;
	cv = "angular v:  " + std::to_string(int(grad)) + "* " + std::to_string(int(min)) + "' " +  std::to_string(int(sec)) + "\"";
	res.push_back(cv);
	
	
	return res;
}

void tinfo::set_track_info(const std::string &_sender,const unsigned long _time, const double _speed, const double _steering_angle, const double _angular_speed)
{
	std::unique_lock<decltype(info_mutex)> lock(info_mutex);
	sender = _sender;
	time = _time;
	speed = _speed;
	steering_angle = _steering_angle;
	angular_speed = _angular_speed;
	kernel->update_info();
}

void tinfo::set_tracktor_info(const SVec& _pos, double _azimuth)
{
	std::unique_lock<decltype(info_mutex)> lock(info_mutex);
	ned_pos = _pos;
	sin_azimuth = _azimuth;
	kernel->update_info();
}






