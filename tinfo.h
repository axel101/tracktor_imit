#ifndef TINFO_H
#define TINFO_H
#include "SMat.h"
#include <vector>
#include <string>
#include <mutex>
/**
 * @todo write docs
 */
class tinfo
{
public:
	tinfo(class tkernel *_k);
	~tinfo();
	std::vector<std::string> get_info();
	void set_track_info(const std::string &_sender,const unsigned long int _time,const double _speed,const double _steering_angle,const double _angular_speed);
	void set_tracktor_info(const SVec &_pos,double _azimuth);
private:
	class tkernel *kernel;
	std::string sender;
	unsigned long int time;
	double speed;
	SVec ned_pos;
	SVec local;
	double sin_azimuth;
	double steering_angle;
	double angular_speed;
	std::mutex info_mutex;
};

#endif // TINFO_H
