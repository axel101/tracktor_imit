#include "tkernel.h"
#include "treader.h"
#include "ttracktor.h"
#include "tsimulator.h"
#include "tinfo.h"

#if defined __unix__ || defined __linux__
#include "X11/txlib_ui.h"
#include <X11/keysymdef.h>
#else
#error "Здесь сделать реализацию под Windows"
#endif

tkernel::tkernel(const std::string &reader_path)
{
	DBG(__PRETTY_FUNCTION__);
#if defined __unix__ || defined __linux__
	main_win = new txlib_ui(this,false);
	info_win = new txlib_ui(this,true);
#else
	#error "Здесь сделать реализацию под Windows"
#endif
	
	info = new tinfo(this);
	reader = new treader(this,reader_path);
	simulator = new tsimulator(this);
// 	cur_source = reader;
	cur_source = simulator;
	
}
void tkernel::start()
{
	std::unique_lock<std::mutex> lock(mutex);
	DBG(__PRETTY_FUNCTION__);
	main_win->show(1200,1000);
	info_win->show(200,100);
	simulator->start();
	reader->start(true);
// 	cur_source->start();
	exit_cond.wait(lock);
	simulator->stop();
	reader->stop();
	main_win->close();
	info_win->close();
}

void tkernel::toggle_source()
{
	cur_source->pause();
	if(cur_source == simulator)
		cur_source = reader;
	else
		cur_source = simulator;
	cur_source->resume();
}


void tkernel::stop()
{
	DBG(__PRETTY_FUNCTION__);
	std::unique_lock<std::mutex> lock(mutex);
	exit_cond.notify_all();
}


tkernel::~tkernel()
{
	DBG(__PRETTY_FUNCTION__);
	delete reader;
	delete simulator;
	delete info_win;
	delete main_win;
	delete info;
}

void tkernel::update_info()
{
	if(info_win)
		info_win->update();
}


void tkernel::key_pressed(unsigned int key)
{
	switch(key)
	{
		case key_space:
		{
			//TODO защитить мьютексом
			cur_source->toggle();
			break;
		}
		case key_enter:
		{
			toggle_source();
			break;
		}
		case key_left:
		{
			simulator->dec_steering_angle();
			break;
		}
		case key_right:
		{
			simulator->inc_steering_angle();
			break;
		}
		case key_up:
		{
			simulator->inc_speed();
			break;
		}
		case key_down:
		{
			simulator->dec_speed();
			break;
		}
		case key_esc:
		{
			stop();
			break;
		}
		default:
		{
			break;
		}
		
	};
}


void tkernel::set_help(const std::vector<std::string> &_help)
{
	main_win->set_help(_help);
}




tpoint operator-(const tpoint &p1,const tpoint &p2)
{
		return {short(p1.x - p2.x),short(p1.y - p2.y)};
}

std::ostream& operator << (std::ostream &stream,const tpoint &obj)
{
    stream <<"("<<obj.x<<","<<obj.y<<")";
    return stream;
}


tpoint operator+(const tpoint& p1, const tpoint& p2)
{
	return {short(p1.x + p2.x),short(p1.y + p2.y)};
}


tpoint &operator+=(tpoint &left,const tpoint &val)
{
	left.x += val.x;
	left.y += val.y;
	return left;
}

bool operator==(const tpoint &left,const tpoint &val)
{
	return (left.x == val.x)&&(left.y == val.y);
}
