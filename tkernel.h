#ifndef TKERNEL_H
#define TKERNEL_H
#include <mutex>
#include <vector>
#include <condition_variable>
class tkernel
{
public:
	tkernel(const std::string &reader_path);
	void start();
	void stop();
	~tkernel();
	void update_info();
	void key_pressed(unsigned int key);
	class tinfo *info;
	class tui *main_win;
	void toggle_source();
	void set_help(const std::vector<std::string> &_help);
private:
	class tui *info_win;
	class treader *reader;
	class tsimulator *simulator;
	class tsource *cur_source;
	std::mutex mutex;
	std::condition_variable exit_cond;
};
#endif // TKERNEL_H
