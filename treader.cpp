#include <unistd.h>
#include <math.h>
#include <iomanip>
#include <stdlib.h>
#include <chrono>
#include <thread>
#include "tkernel.h"
#include "treader.h"
#include "tinfo.h"
#include "ttracktor.h"
#include "tui.h"
treader::treader(class tkernel *_k,const std::string &_path)
:tsource(_k),data_file(_path)
{
	name = "reader";
	help.push_back("Using " + name);
	help.push_back("Space - pause reading");
	help.push_back("Enter - pause and switch to simulator");
	help.push_back("Esc - exit programm");
	
	DBG(__PRETTY_FUNCTION__);
	//За локальную точку взял первую из лог-файла
// 	loc.x0 = 2829169.83;
// 	loc.x1 = 2208058.32;
// 	loc.x2 = 5255180.65;
// 	
	//В качестве локальной точки берём центральную точку (центр по каждой координате)
// 	loc.x0 = 2829201.51;
// 	loc.x1 = 2208001.92;
// 	loc.x2 = 5255192.89;
	//В качестве локаьлной точкиберём наименьшую из точек
	loc.x0 = 2829169.83;
	loc.x1 = 2208058.32;
	loc.x2 = 5255180.65;
	
	
	
	ecef2geo(loc.x0,loc.x1,loc.x2,loc_geo);
	DBG(std::setprecision(15)<<"Local ECEF x = "<<loc.x0<<", y = "<<loc.x1<<", x = "<<loc.x2);
	DBG("Local GEO  x = "<<loc_geo.x0<<", y = "<<loc_geo.x1<<", x = "<<loc_geo.x2);
	
}

treader::~treader()
{
	DBG(__PRETTY_FUNCTION__);
}

void treader::ecef2ned(double x, double y, double z, SVec &point)
{
	SMat m(	-sin(loc_geo.x0)*cos(loc_geo.x1),	-sin(loc_geo.x1),	-cos(loc_geo.x0)*cos(loc_geo.x1),
			-sin(loc_geo.x0)*sin(loc_geo.x1),	cos(loc_geo.x1),		-cos(loc_geo.x0)*sin(loc_geo.x1),
			cos(loc_geo.x0),					0,					-sin(loc_geo.x0));
	SVec src(x,y,z);
	point = m*(src - loc);
	double tmp = point.x0;
	point.x0 = point.x1;
	point.x1 = tmp;
// 	DBG("x "<<x<<" y "<<y<<" z "<<z);
// 	DBG("north "<<point.x1<<" east "<<point.x0<<" down "<<point.x2);
	
}

void treader::ecef2geo(double &x,double &y,double &z,SVec &point)
{
	
	double z_2 = z*z;
	double r_2 = x*x + y*y;
	double r = sqrt(r_2);
	double F = 54*_B_2*z_2;
	double G = r_2 + (1 - _E_2)*z_2 - _E_2*(_A_2 - _B_2);
	double c = (_E_4*F*r_2)/pow(G,3);
	double s = pow((1 + c + sqrt(c*c + 2*c)),1/3.);
	double P = F/(3*pow(s + 1/s + 1,2)*G*G);
	double Q = sqrt(1 + 2*_E_4*P);
	double r0 = - (P*_E_2*r)/(1.+Q) + sqrt(0.5*_A_2*(1 + 1/Q) - (P*(1 - _E_2)*z_2)/(Q*(1 + Q)) - 0.5*P*r_2);
	double tmp = pow(r - _E_2*r0,2);
	//Нужен только для высоты
// 	double U = sqrt(tmp + z_2);
	double V = sqrt(tmp + (1 - _E_2)*z_2);
	double z0 = (_B_2*z)/(_A*V);
	point.x0 = atan2(z + _E_R2*z0,r);
	
	point.x1 = atan2(y,x);
// 	DBG(std::setprecision(15)<<" x "<<x <<" y "<<y<<" z "<<z);
// 	DBG(std::setprecision(15)<<" lat "<<point.x0<<" lon "<<point.x1);
			
// 	point -= loc;
	return;
}


void treader::thread_func()
{
	DBG(__PRETTY_FUNCTION__);
	unsigned long cur_time,next_time;
	double x,y,z,pad1,pad2,pad3,speed,pad4,pad5,azimuth,pad6,pad7;
	SVec p;
	running = 1;
	data_file >> cur_time >> x >> y >> z >> pad1 >> pad2 >> pad3 >> speed >> pad4 >> pad5 >> azimuth >> pad6 >> pad7;
	
// 	kernel->info_win->set_local(loc);

	/*
	min_x = x;
	min_y = y;
	max_x = x;
	max_y = y;
	max_z = z;
	max_z = z;
	*/
// 	DBG(std::setprecision(25)<<" x "<<p.x0 <<" y "<<p.x1);
	while(running)
	{

		if(data_file >> next_time >> x >> y >> z >> pad1 >> pad2 >> pad3 >> speed >> pad4 >> pad5 >> azimuth >> pad6 >> pad7)
		{
			unsigned int next_step = next_time - cur_time;
			std::this_thread::sleep_for(std::chrono::milliseconds(next_step));
			cur_time = next_time;
			SVec p;
			ecef2ned(x,y,z,p);
			
			
			tracktor->move(p,speed);
			kernel->info->set_track_info(name,cur_time,speed,0,0);
// 			kernel->info_win->draw_info(cur_time,speed,p);
			/*
			min_x = min_x>x?x:min_x;
			min_y = min_y>y?y:min_y;
			max_x = max_x<x?x:max_x;
			max_y = max_y<y?y:max_y;
			min_z = min_z>z?z:min_z;
			max_z = max_z<z?z:max_z;
			*/
			std::unique_lock<decltype(pause_mutex)> pause_lock(pause_mutex);
		}
		else
		{
			running = false;
		}
	}
	/*
	DBG(std::setprecision(25)<< " min_x " << min_x << " max_x " << max_x<< " mid "<<(max_x + min_x)/2. << " range "<<(max_x - min_x));
	DBG(std::setprecision(25)<< " min_y " << min_y << " max_y " << max_y<< " mid "<<(max_y + min_y)/2. << " range "<<(max_y - min_y));
	DBG(std::setprecision(25)<< " min_z " << min_z << " max_z " << max_z<< " mid "<<(max_z + min_z)/2. << " range "<<(max_z - min_z));
	*/
}

