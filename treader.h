#ifndef TREADER_H
#define TREADER_H
#include <fstream>
#include "SMat.h"
#include "tsource.h"
/**
 * @todo write docs
 */
#define MID_X 55.67001078098952859818382
#define MID_Y 37.96871897446323629310427
class treader : public tsource
{
public:
    /**
     * Default constructor
     */
    treader(class tkernel *_k,const std::string &_path);

    /**
     * Destructor
     */
    virtual ~treader();
	void thread_func() override;
	void ecef2geo(double &x,double &y,double &z,SVec &p);
	/**
	 * @brief Перевод из ECEF в NED
	 * Внимание! поскольку в gui принято первой кординатой указывать X, а в NED первой координатой идёт North (Y), то эта функция будет разворачивать координаты в экранный вид
	 * @param x EXEF x координата.
	 * @param y EXEF y координата.
	 * @param z EXEF z координата.
	 * @param p сюда будет записан результат в NED координатах (x0 - Воток, x1 - Север)
	 */
	void ecef2ned(double x, double y, double z,SVec &point);
// 	int start() override;
// 	void stop() override;
private:
	std::ifstream data_file;
	
// 	double min_x,max_x,min_y,max_y,min_z,max_z;


};

#endif // TREADER_H
