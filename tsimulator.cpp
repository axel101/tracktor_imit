#include "tsimulator.h"
#include "ttracktor.h"
#include <math.h>
#include "tkernel.h"
#include "tui.h"
#include "tinfo.h"
tsimulator::tsimulator(class tkernel *_k)
:tsource(_k),steering_angle(0),azimuth(0),speed(0),pos(0,0,0)
{
	name = "simulator";
	help.push_back("Using " + name);
	help.push_back("Key Up - icrease speed");
	help.push_back("Key Down - reduce speed (Allows negative speed)");
	help.push_back("Key left - Turn steering wheel left at 10 degerees (maximum 50");
	help.push_back("Key right - Turn steering wheel right at 10 degerees (minimum -50)");
	help.push_back("Space - pause simulation");
	help.push_back("Enter - pause and switch to reader");
	help.push_back("Esc - exit programm");
	
	DBG(__PRETTY_FUNCTION__);
}

tsimulator::~tsimulator()
{
	DBG(__PRETTY_FUNCTION__);
}


void tsimulator::thread_func()
{
	running = 1;
	unsigned long int time = 0;
	speed = 0;//0.01;
	
	//Вычисляем таймаут из скорости и приращения пройденного растояния за шаг
	unsigned int timeout = 5;
	steering_angle = 0;//
	while(running)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(timeout));
		std::unique_lock<decltype(params_mutex)> lock(params_mutex);
		time += timeout;
		//Вычисляем новую точку
		pos.x1 += speed*cos(azimuth);
		pos.x0 += speed*sin(azimuth);
		double a_speed = speed*(tan(steering_angle)/TRACKTOR_H);
		azimuth += a_speed;
		
		tracktor->move(pos,speed);
		kernel->info->set_track_info(name,time,speed,steering_angle,a_speed);
		
		
		std::unique_lock<decltype(pause_mutex)> pause_lock(pause_mutex);
// 		kernel->info_win->draw_info(steps,speed,pos);
	}
}

void tsimulator::dec_speed(double val)
{
	DBG("speed down at "<<val);
	std::unique_lock<decltype(params_mutex)> lock(params_mutex);
	speed -= val;
}

void tsimulator::inc_speed(double val)
{
	DBG("speed up at "<<val);
	std::unique_lock<decltype(params_mutex)> lock(params_mutex);
	speed += val;
}


void tsimulator::dec_steering_angle(double dec_val)
{
	DBG("steering down at "<<dec_val);
	double a = dec_val*M_PI/180.;
	if(steering_angle <= -50*M_PI/180.)
	{
		DBG("Can not reduce angle");
		return;
	}
	std::unique_lock<decltype(params_mutex)> lock(params_mutex);
	steering_angle -= a;
}

void tsimulator::inc_steering_angle(double inc_val)
{
	DBG("steering up at "<<inc_val);
	double a = inc_val*M_PI/180.;
	if(steering_angle >=50*M_PI/180.)
	{
		DBG("Can not increase angle");
		return;
	}
	std::unique_lock<decltype(params_mutex)> lock(params_mutex);
	steering_angle += a;
}




