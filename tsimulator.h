#ifndef TSIMULATOR_H
#define TSIMULATOR_H
#include <mutex>
#include "tsource.h"
class tsimulator : public tsource
{
public:
	tsimulator(tkernel* _k);
	void thread_func() override;
	virtual ~tsimulator();
	/**
	 * @brief Увеличить угол поворота колеса
	 * Увеличивает угол поворота колеса.
	 * @param inc_val Угол на который необходимо увеличить угол поворота. Задаётся в градусах
	 */
	void inc_steering_angle(double inc_val = 15);
	/**
	 * @brief Уменьшить угол поворота колеса
	 * Уменьшает угол поворота колеса.
	 * @param dec_val Угол на который необходимо увеличить угол поворота. Задаётся в градусах
	 */
	void dec_steering_angle(double dec_val = 15);
	/**
	 * @brief Увеличить скорость трактора
	 * 
	 * @param val Величина на которую необходимо увеличить скорость трактора
	 */
	void inc_speed(double val = 0.01);
	/**
	 * @brief Уменьшить скорость трактора
	 * 
	 * @param val Величина на которую необходимо уменьшить скорость трактора
	 */
	void dec_speed(double val = 0.01);
private:
	double steering_angle;
	double azimuth;
	double speed;
	SVec pos;
	std::mutex params_mutex;
	

};

#endif // TSIMULATOR_H
