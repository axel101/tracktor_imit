#include "tsource.h"
#include "tkernel.h"
#include "tui.h"
#include "ttracktor.h"
#include "SMat.h"
#include <math.h>



tsource::tsource(class tkernel *_k)
:kernel(_k),paused(false),running(0)
{
	DBG(__PRETTY_FUNCTION__);
	tracktor = new ttracktor(this);
}

tsource::~tsource()
{
	DBG(__PRETTY_FUNCTION__);
	delete tracktor;
}



int tsource::start(bool paused)
{
	if(paused)
	{
		pause();
	}
	else
	{
		resume();
	}
// 	kernel->set_help(help);
	thread = std::thread(&tsource::thread_func,this);
	return 0;
}



void tsource::stop()
{
	DBG(__PRETTY_FUNCTION__);
	running = false;
	pause_mutex.unlock();
	thread.join();
}

void tsource::pause()
{
	if(!paused)
	{
		DBG("Suspending");
		pause_mutex.lock();
		paused = true;
	}
}

void tsource::resume()
{
	kernel->set_help(help);
	if(paused)
	{
		DBG("Resuming");
		pause_mutex.unlock();
		paused = false;
	}
}

void tsource::toggle()
{
	DBG("Toggling");
	if(paused)
		resume();
	else
		pause();
}








