#ifndef TSOURCE_H
#define TSOURCE_H
#include <thread>
#include <mutex>
#include <vector>
#include "SMat.h"
#include "defines.h"
// #include <bits/types/sig_atomic_t.h>
class tsource
{
public:
	/**
		* Default constructor
		*/
	tsource(class tkernel *_k);
	/**
	 * @brief Запустить поток источника данных
	 * 
	 * @return int
	 */
	virtual int start(bool paused = false);
	virtual void stop();
	void pause();
	void resume();
	void toggle();
	virtual ~tsource();
	virtual void thread_func() = 0;
	
	
	/**
	 * @brief Локальные координаты
	 * Локальные координаты относительно которых будет производиться смешение при масштабировании
	 */
	SVec loc;
	SVec loc_geo;
	class tkernel *kernel;
private:
	std::thread thread;
	bool paused;
	
protected:
	unsigned char running;
	class ttracktor *tracktor;
	std::mutex pause_mutex;
	std::string name;
	std::vector<std::string> help;
};

#endif // TTRACKTOR_H
