#include "tinfo.h"
#include "ttracktor.h"
#include "tkernel.h"
#include "treader.h"
#include "tsource.h"
#include "tui.h"



ttracktor::ttracktor(class tsource *_k)
:source(_k),ned_pos(SVec(2,2,2)),
body({SVec(0,TRACKTOR_H,0),SVec(-TRACKTOR_H/3.,0,0),SVec(TRACKTOR_H/3.,0,0)}),
cur_position({{0,0},{0,0},{0,0}}),sin_az(0),cos_az(0)
{
	
}

ttracktor::~ttracktor()
{

}
void ttracktor::move(const SVec &new_ned_pos,double speed)
{
	if((ned_pos.x0 == new_ned_pos.x0)&&(ned_pos.x1 == new_ned_pos.x1))
	{
		return;//Трактор не двигался
	}
	//Вычисляем угол отклонения трактора
	//Вычисляем катеты и гипотинузу для синуса и косинуса отклонения от оси Y
	double far_cat = new_ned_pos.x0 - ned_pos.x0;
	double near_cat = new_ned_pos.x1 - ned_pos.x1;
	if(speed<0)
	{
		far_cat = -far_cat;
		near_cat = -near_cat;
	}
	double hyp = hypot(near_cat,far_cat);
	
	//Синус азимута
	sin_az = far_cat/hyp;
	//Косинус азимута
	cos_az = near_cat/hyp;
	
	ned_pos = new_ned_pos;
// 	DBG("far_cat = "<<far_cat<<" near_cat "<<near_cat<<" angle"<<asin(sin_az)*180/M_PI);
	//Поворачиваем трактор на этот угол
	//Матрица поворота по часовой стрелке от оси Y
	SMat rotate_mtx(	cos_az,		sin_az,	0,
						-sin_az,	cos_az,	0,
						0,			0,		1);
	SVec v;
	
	tpoint new_screen_pos;
	//В цикле поворачиваем каждую точку трактора на азимут
	for(int i = 0; i<BODY_LENGTH;i++)
	{
		//Поворачиваем точку трактора (body положение трактора носом по оси Y)
		v = rotate_mtx*body[i];
		//Смещаем точку
		v += new_ned_pos;
		//Масштабируем на экран
		projection(v,new_screen_pos);
		//Прибавляем к относительной координате трактора смещение центра трактора
		cur_position[i].x = /*v.x0 + */new_screen_pos.x;
		cur_position[i].y = /*v.x1 + */new_screen_pos.y;
	}
	
	source->kernel->main_win->draw_tracktor(cur_position);
	source->kernel->info->set_tracktor_info(ned_pos,sin_az);
}


void ttracktor::projection(const SVec& ned,tpoint &p)
{
	//Проецируем реальные координаты на экранные. В качестве проекции используем простое масштабирование (Число подобрано так чтобы было удобно смотреть на экране)
	p.x = ned.x0*10;
	p.y = ned.x1*10;
// 	DBG("Screen x = "<<p.x<<" screen y = "<<p.y);
}
