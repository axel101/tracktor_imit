#ifndef TTRACKTOR_H
#define TTRACKTOR_H
#include "SMat.h"
#include "defines.h"


/**
 * @todo write docs
 */
class ttracktor
{
public:
	/**
		* Default constructor
		*/
	ttracktor(class tsource *_k);

	/**
		* Destructor
		*/
	~ttracktor();
	/**
	 * @brief Сдвинуть и нарисовать трактор на новых координатах
	 * 
	 * @param new_point Новые географические координаты
	 * @param speed Скорость
	 */
	void move(const SVec &new_point,double speed);
	/**
	 * @brief Проецирование географических координат на экранные
	 * 
	 * @param geo географические координаты
	 * @param p экранные координаты
	 */
	void projection(const SVec &geo,tpoint &p);
private:
	class tsource *source;
	//Координаты центра трактора
	SVec ned_pos;
	
	//Описывает координаты трактора в вертикальном положении. Координаты относительно центра.
	SVec body[BODY_LENGTH];
	//Описывает координаты трактора в текущей позиции. Координаты абсолютные.
	tpoint cur_position[BODY_LENGTH];
	//Синус азимута
	double sin_az;
	//Синус азимута
	double cos_az;

};

#endif // TTRACKTOR_H
