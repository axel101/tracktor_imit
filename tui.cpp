#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "tui.h"
#include "tsource.h"
#include "tkernel.h"

tui::tui(class tkernel *_k,bool _draw_info)
:tracktor(0),draw_info(_draw_info), kernel(_k)
{
	DBG(__PRETTY_FUNCTION__);
// 	tracktor = (tpoint*)calloc(tr_pts,sizeof(tpoint));
}

tui::~tui()
{
	DBG(__PRETTY_FUNCTION__);
	if(tracktor)
		delete[] tracktor;
}

void tui::start_gui_thread()
{
	gui_thread = std::thread(&tui::gui_handler,this);
}

// void tui::set_local(const SVec &_local)
// {
// 	info_mutex.lock();
// 	local = _local;
// 	info_mutex.unlock();
// }
/*
int tui::rad_to_grad_str(double rad, char* str, int max_size)
{
	double grad = 180*rad/M_PI;
	double min = (grad - (int)grad)*60.;
	double sec = (min - (int)min)*60.;
	return snprintf(str,max_size,"%3d* %2d' %2.3lf\"",(int)grad,(int)min,sec);
}



void tui::draw_info(unsigned long int _time,double _speed,const SVec &_point)
{
	//Рисуем в потоке GUI чтобы разгрузить поток вычислений
	info_mutex.lock();
	time = _time;
	speed = _speed;
	point = _point;
	info_mutex.unlock();
	update();
}*/
/*
void tui::draw_body_angle(double sin_ba)
{
	//Рисуем в потоке GUI чтобы разгрузить поток вычислений
	info_mutex.lock();
	body_angle = asin(sin_ba);
	info_mutex.unlock();
	update();
}*/

void tui::set_help(const std::vector<std::string>& _help)
{
	help_mutex.lock();
	help = _help;
	help_mutex.unlock();
	update();
}


void tui::draw_tracktor(tpoint *_t)
{
	tracktor_mutex.lock();
	if(!tracktor)
		tracktor = new tpoint[BODY_LENGTH + 1];//(tpoint *)malloc((BODY_LENGTH + 1)*sizeof(tpoint));
	
	//TODO Если перейти на двойной буффер, то можно избавиться от этого memcpy
	//memcpy(tracktor,_t,BODY_LENGTH*sizeof(tpoint));
	for(int i = 0;i<BODY_LENGTH;i++)
	{
		//Смещаем центр координат не в центр, а правее, потому как трактор уходит за пределы экрана (по крайней мере в моём разрешении)
// 		DBG("in X = "<<_t[i].x<<", Y"<<_t[i].y);
		tracktor[i].x = _t[i].x + width/1.2;
		//Зеркалируем Y, потому что на экране координата Y смотрит вниз
		tracktor[i].y = height - (_t[i].y + height/2.);
// 		DBG("Real X = "<<tracktor[i].x<<", y"<<tracktor[i].y<<" w "<<width<<" h "<<height);
		
		
		
	}

	//Замыкаем фигуру
	tracktor[BODY_LENGTH] = tracktor[0];
	
	/*
	for(int i = 0;i<BODY_LENGTH;i++)
	{
		DBG("draw pos["<<i<<"] = "<<tracktor[i]);
	}
	*/
	
	tracktor_mutex.unlock();
	update();
}
/*
void tui::draw_track(tpoint* _t)
{
	//Жёстко забито на 2 точки
	tracktor_mutex.lock();
	memcpy(track,_t,(2) * sizeof(tpoint));
	tracktor_mutex.unlock();
	update();
}

*/
