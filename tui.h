#ifndef TUI_H
#define TUI_H
#include <thread>
#include "SMat.h"
#include <mutex>
#include <string>
#include <vector>
#include "defines.h"
class tui
{
public:
	tui(class tkernel *_k,bool _draw_info);
	virtual int show(int w,int h) = 0;
	virtual void close() = 0;
	/**
	 * @brief Нарисовать трактор. 
	 * Копирует массив координат трактора длинно BODY_LENGTH и рисует его на экране
	 * 
	 * @param tracktor Массив координат трактора. Должен быть не меньше BODY_LENGTH
	 */
	void draw_tracktor(tpoint *tracktor);
	void set_local(const SVec &local);
	virtual ~tui();
	tpoint *tracktor;
	virtual void update() = 0;
	void set_help(const std::vector<std::string> &_help);
protected:
	virtual void gui_handler() = 0;
	bool draw_info;
	std::vector<std::string> info;
	void start_gui_thread();
	int width;
	int height;
	std::thread gui_thread;
	std::mutex tracktor_mutex;
	std::mutex help_mutex;
	std::vector<std::string> help;
	class tkernel *kernel;
	

};

#endif // TUI_H
